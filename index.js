'use strict'
const WORKERS = process.env.WEB_CONCURRENCY || 1
const IS_PRODUCTION = process.env.NODE_ENV === 'production'
const os = require('os')
const fs = require('fs')
const config = require('./config')
const version = require('./package.json').version
const File = require('./models/File')
function start () {
  const restify = require('restify')
  const mongoose = require('mongoose')
  mongoose.connect(config.MONGODB_URI, { promiseLibrary: require('bluebird') })
  let server = restify.createServer({
    name: 'Confberus',
  version})

  server.use(restify.acceptParser(server.acceptable))
  server.use(restify.queryParser())
  server.use(restify.bodyParser({
    maxBodySize: 0,
    mapParams: true,
    mapFiles: false,
    overrideParams: false,
    keepExtensions: true,
    uploadDir: os.tmpdir(),
    multiples: true
  }))
  server.get('/', (req, res) => {
    res.setHeader('content-type', 'text/plain')
    res.send('Confberus running')
  })
  let checkVersion = (params, next) => {
    if (params.version === 'latest') {
      delete params.version
      File.aggregate([
        {$match: params},
        {
          $group: {
            _id: '$version',
            date: {$max: '$created_at'}
          }
        },
        {$sort: {date: -1}}
      ], (err, results) => {
        if (err) return next(err)
        if (!results.length) {
          return next(new restify.NotFoundError())
        }
        params.version = results[0]._id
        next()
      })
      return
    }
    next()
  }
  // Serve resource notes with fine grained mapping control 
  server.get('/projects', (req, res, next) => {
    File.distinct('project', (err, projects) => {
      next.ifError(err)
      res.send({ projects})
    })
  })

  server.get('/projects/:project', (req, res, next) => {
    File.findOne(req.params, (err, t) => {
      next.ifError(err)
      if (!t) {
        return next(new restify.NotFoundError())
      }
      File.distinct('environment', req.params, (err, versions) => {
        next.ifError(err)
        res.send({ versions})
      })
    })
  })

  server.get('/projects/:project/:environment/', (req, res, next) => {
    File.findOne(req.params, (err, t) => {
      next.ifError(err)
      if (!t) {
        return next(new restify.NotFoundError())
      }
      File.distinct('version', req.params, (err, environments) => {
        next.ifError(err)
        res.send({ environments})
      })
    })
  })

  server.get('/projects/:project/:environment/:version', (req, res, next) => {
    if (typeof req.params.contentType === 'string' && !req.params.contentType.trim().length) {
      delete req.params.contentType
    }
    if (typeof req.params.dir === 'string') {
      let dir = req.params.dir.trim()
      if (!dir.startsWith('/')) {
        dir = '/' + dir
      }
      switch (dir.length) {
        case 0:
          break
        default:
          dir = '^' + dir
          req.params.name = new RegExp(dir)
      }
      delete req.params.dir
    }

    if (req.params.contentType && req.params.contentType.startsWith('/')) {
      req.params.contentType = new RegExp(req.params.contentType.substring(1, req.params.contentType.length - 1))
    }
    checkVersion(req.params, (err) => {
      next.ifError(err)
      File.aggregate([
        {
          $match: req.params
        },
        {
          $group: {
            _id: { name: '$name', contentType: '$contentType',version: '$version' },
            date: { $max: '$created_at' }
          }
        },
        {
          $project: {
            _id: false,
            name: '$_id.name',
            version: '$_id.version',
            contentType: '$_id.contentType',
            date: '$date'
          }
        },
        { $sort: { name: 1, date: -1 } }
      ], (err, files) => {
        next.ifError(err)
        res.send({ files})
      })
    })
  })

  server.get(/^\/projects\/([a-zA-Z0-9_]+)\/([a-zA-Z0-9_]+)\/(latest|[0-9]+(\.[0-9]+){0,3})\/(.*)/, (req, res, next) => {
    if (typeof req.params.contentType === 'string') {
      delete req.params.contentType
    }
    let params = {
      project: req.params[0],
      environment: req.params[1],
      version: req.params[2],
      name: req.params[4]
    }
    params.name = '/' + params.name
    checkVersion(params, (err) => {
      next.ifError(err)
      File.findOne(params, (err, file) => {
        next.ifError(err)
        res.setHeader('content-type', file.contentType)
        res.send(file.file.toString())
      })
    })
  })

  server.post('/projects', (req, res, next) => {
    let params = {}
    for (let p in req.body) {
      params[p] = req.body[p]
    }
    if (typeof req.files.file === 'undefined') {
      return next(new restify.ConflictError('Missing file'))
    }

    params.contentType = req.files.file.type
    fs.readFile(req.files.file.path, (err, data) => {
      next.ifError(err)
      params.file = data
      console.log(req.params)
      File.create(params, (err, result) => {
        console.log(err)
        next.ifError(err)
        res.send(201, result)
      })
    })
  })

  server.listen(process.env.PORT || 5000, () => {
    console.log('%s listening at %s', server.name, server.url)
  })
}
let throng = require('throng')

throng({
  workers: WORKERS,
  lifetime: Infinity
}, start)
