'use strict'
const mongoose = require('mongoose')
let FileSchema = new mongoose.Schema({
  project: {
    type: String,
    required: true,
    trim: true,
    validate: {
      validator: function (v) {
        return /[a-zA-Z0-9-_]+/.test(v)
      },
      message: '{VALUE} contains invalid characters'
  }},
  environment: {
    type: String,
    required: true,
    trim: true,
    validate: {
      validator: function (v) {
        return /[a-zA-Z0-9-_]+/.test(v)
      },
      message: '{VALUE} contains invalid characters'
  }},
  version: {
    type: String,
    required: true,
    trim: true,
    validate: {
      validator: function (v) {
        return /[0-9]+(\.[0-9]+){0,3}/.test(v)
      },
      message: '{VALUE} invalid version format'
    }
  },
  contentType: {
    type: String,
    required: true,
    trim: true
  },
  name: {type: String,required: true},
  created_at: {type: Date,default: Date.now()},
  file: {type: Buffer,required: true}
})
FileSchema.pre('save', function (next) {
  if (!this.name.startsWith('/')) {
    this.name = '/' + this.name
  }
  next()
})
FileSchema.index({project: 1,environment: 1,name: 1,version: 1,date: -1})
module.exports = mongoose.model('files', FileSchema)
