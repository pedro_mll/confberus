'use strict'
module.exports = function (grunt) {
  const ONE_YEAR = 60 * 60 * 24 * 350
  const LINES = parseFloat(process.env.LINES) || 10
  const STATEMENTS = parseFloat(process.env.STATEMENTS) || 10
  const crypto = require('crypto')
  const path = require('path')
  const fileSystem = require('fs')
  const IS_DEV = process.env.NODE_ENV !== 'production'

  let ProvidePlugin = require('webpack/lib/ProvidePlugin'),
    webpack = require('webpack'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    LessPluginAutoprefix = require('less-plugin-autoprefix'),
    LessPluginCleanCSS = require('less-plugin-clean-css'),
    sourceMap = IS_DEV ? '?sourceMap' : '',
    pkg = require('./package.json'),
    hash = crypto.createHash('md5')
  hash.update(pkg.version)

  let version = hash.digest('hex'),
    js = {
      'html-minify-loader': {
        quotes: true
      },
      lessLoader: {
        lessPlugins: [
          new LessPluginAutoprefix({
            browsers: ['last 5 versions', 'Safari >= 5', 'iOS >= 5', 'Android >= 2', 'ie >= 9', 'opera >= 12', 'Firefox >= 20', 'Chrome >= 20']
          })
        ]
      },
      entry: {
        bundle: './front/src/index.js'
      },
      output: {
        path: `./assets/`,
        publicPath: `/assets/`,
        filename: `${version}.js`,
        sourceMapFilename: `${version}.js.map`
      },
      module: {
        loaders: [
          { test: /\.json$/, loader: 'json' },
          { test: /\.json\.js$/, loader: 'tojson' },
          { test: /\.jsx?$/, loader: 'babel', exclude: /node_modules/, query: { presets: ['es2015', 'stage-0'], plugins: ['transform-decorators-legacy', 'transform-runtime'] } },
          { test: /\.less$/, loader: ExtractTextPlugin.extract('style-loader', `css-loader${sourceMap}!less-loader${sourceMap}`) },
          { test: /\.css?$/, loader: 'style!css' },
          { test: /favicon\.(ico|png)$/, loader: 'file-loader?name=favicon.[ext]' },
          {
            test: /\.(png|gif|jpe?g)$/i,
            loaders: [
              'url?limit=8192',
              'image-webpack?{progressive:true, optimizationLevel: 7, interlaced: false, pngquant:{quality: "65-90", speed: 4}}'
            ]
          },
          { test: /\.woff2(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url?limit=10000&mimetype=application/font-woff2' },
          { test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url?limit=10000&mimetype=application/font-woff' },
          { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file' },
          { test: /\.html$/, loader: 'html?minify=true' }
        ]
      },
      resolve: {
        extensions: ['', '.jsx', '.js'],
        root: [
          path.resolve('node_modules'),
          path.resolve('front/src'),
          path.resolve('front/styles'),
          path.resolve('front/public')
        ]
      },
      plugins: [
        new webpack.ProvidePlugin({
          $: 'jquery',
          jQuery: 'jquery',
          'window.jQuery': 'jquery'
        }),
        new webpack.EnvironmentPlugin([
          'NODE_ENV'
        ]),
        new webpack.DefinePlugin({
          DEV: IS_DEV
        }),

        new ExtractTextPlugin(`${version}.css`),

        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.DedupePlugin()

      ],
      stats: {
        // Configure the console output
        colors: true,
        timing: true,
        hash: true
      }
  }
  if (IS_DEV) {
    js.plugins.unshift(new webpack.HotModuleReplacementPlugin())
    js.devtool = 'source-map'
  } else {
    var ugly = new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
          drop_console: true
        },
        screwIE8: true
      }),
      less = new LessPluginCleanCSS({
        advanced: false,
        rebase: false
      })
    js.plugins.push(ugly)
    js.lessLoader.lessPlugins.push(less)
  }
  grunt.initConfig({
    webpack: {js},
    watch: {
      front: {
        files: 'front/**/*',
        tasks: ['webpack:js'],
        options: {
          livereload: 9000
        }
      },
      server: {
        files: 'server/**/*',
        tasks: ['express']
      }
    },
    compress: {
      production: {
        options: {
          mode: 'gzip'
        },
        expand: true,
        cwd: 'assets/',
        src: ['**/*'],
        dest: 'dist/',
        rename: (dest, src) => {
          dest += src
          if (!grunt.file.isDir(src))
            dest += '.gz'
          return dest
        }
      }
    },
    clean: {
      public: ['assets', 'dist']
    },
    env: {
      test: {
        NODE_ENV: 'test'
      }
    },
    express: {
      options: {
        port: 3000,
        node_env: 'development',
        debug: true
      },
      dev: {
        options: {
          script: 'server/index.js'
        }
      }

    }
  })

  grunt.loadNpmTasks('grunt-webpack')
  grunt.loadNpmTasks('grunt-env')
  grunt.loadNpmTasks('grunt-contrib-clean')
  grunt.loadNpmTasks('grunt-contrib-compress')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-express-server')

  let production = []

  if (process.env.CI_BRANCH === 'master') {
    production = ['clean', 'webpack:js', 'compress', 'clean']
  }
  grunt.registerTask('production', production)

  grunt.registerTask('dev', ['clean', 'webpack', 'express', 'watch'])
}
